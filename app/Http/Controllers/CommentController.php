<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentStoredEvent;
//use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
//use App\Mail\CommentAuthorMail;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function index(Request $request){
        $post_id = $request->post_id;
        $comments = Comment::where('post_id',$post_id)->latest()->get();

        return response()->json([

            'sukses'    => true,
            'komentar'     => 'data daftar komentar berhasil ditampilkan',
            'data'      => $comments

        ]);
    }

    public function store(Request $request){
        $allRequest = $request->all();  

        $validator = Validator::make($allRequest,[
            'content' => 'required',
            'post_id' => 'required',

        ]);

        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comments = Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id,
        ]);

        //memanggil event CommentStoredEvent
        event(new CommentStoredEvent(($comments)));

        //ini utk yg punya post
        //Mail::to($comments->post->user->email)->send(new PostAuthorMail($comments));
        //ini utk yg punya comment
        //Mail::to($comments->user->email)->send(new CommentAuthorMail($comments));

        if ($comments){

        return response()->json([
            'sukses'    => true,
            'komentar'     => 'data komentar berhasil ditampilkan',
            'data'      => $comments
        ],200);
        }

        return response()->json([
            'sukses'    => false,
            'komentar'     => 'data komentar gagal dibuat',
        ],409);
        

    }

    public function show($id){

        $comments = Comment::find($id);

        if($comments){
            return response()->json([
                'sukses'    => true,
                'komentar'     => 'data komentar berhasil ditampilkan',
                'data'      => $comments
            ],200);
        }

        return response()->json([
            'sukses' => false,
            'komentar' => 'Data dengan id : ' . $id. ' berhasil diupdate',
        ],404);

    }

    public function update(Request $request, $id){
        $allRequest = $request->all();

        $validator = Validator::make($allRequest,[
            'content' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors(),400);
        }

        $comments = Comment::find($id);

        if($comments){

            $user = auth()->user();

            if($comments->user_id != $user->id)
            {
                return response()->json([
                    'sukses' => false,
                    'pesan' => 'Data post bukan milik user login',
                ],403);
            }

            $comments->update([
                'content' => $request->content,
                
            ]);

            return response()->json([
                'sukses' => true,
                'komentar' => 'Data komentar berhasil diupdate',
                'data' => $comments
            ]);
        }

        return response()->json([
            'sukses' => false,
            'komentar' => 'Data dengan id : '.$id.' tidak ditemukan',
        ],404);
    }

    public function destroy($id){
        $comments = Comment::find($id);

        if($comments){

            $user = auth()->user();

            if($comments->user_id != $user->id)
            {
                return response()->json([
                    'sukses' => false,
                    'pesan' => 'Data post bukan milik user login',
                ],403);
            }

            $comments->delete();


            return response()->json([
                'sukses'    => true,
                'komentar'     => 'data komentar berhasil didelete',
                'data'      => $comments
            ],200);
        }

        return response()->json([
            'sukses' => false,
            'komentar' => 'Data dengan id : '.$id.' tidak ditemukan',
        ],404);

    }
    
}
