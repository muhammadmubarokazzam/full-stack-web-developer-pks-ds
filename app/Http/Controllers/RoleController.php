<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index(){

        $posts = Role::latest()->get();

        return response()->json([

            'sukses'    => true,
            'isi'     => 'data daftar role berhasil ditampilkan',
            'data'      => $posts

        ]);
    }

    public function store(Request $request){
        $allRequest = $request->title;        
        $validator = Validator::make($allRequest,[
            'name' => 'required',

        ]);

        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $post = Role::create([
            'name' => $request->name,
        ]);

        return response()->json([
            'sukses'    => true,
            'isi'     => 'data daftar role berhasil ditampilkan',
            'data'      => $post
        ],200);

        return response()->json([
            'sukses'    => false,
            'isi'     => 'data daftar role berhasil ditampilkan',
        ],409);

    }

    public function show($id){

        $post = Role::find($id);

        if($post){
            return response()->json([
                'sukses'    => true,
                'isi'     => 'data role berhasil ditampilkan',
                'data'      => $post
            ],200);
        }

        return response()->json([
            'sukses' => false,
            'isi' => 'Data dengan id : '.$id.' berhasil diupdate',
        ],404);

    }

    public function update(Request $request, $id){
        $allRequest = $request->all();

        $validator = Validator::make($allRequest,[
            'name' => 'required',
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors(),400);
        }

        $post = Role::find($id);

        if($post){

            $post->update([
                'name' => $request->name,
            ]);

            return response()->json([
                'sukses' => true,
                'isi' => 'Data dengan judul : '.$post->title.' berhasil diupdate',
                'data' => $post
            ]);
        }

        return response()->json([
            'sukses' => false,
            'isi' => 'Data dengan id : '.$id.' berhasil diupdate',
        ],404);
    }

    public function destroy($id){
        $post = Role::find($id);

        if($post){

            $post->delete();


            return response()->json([
                'sukses'    => true,
                'isi'     => 'data role berhasil didelete',
                'data'      => $post
            ],200);
        }

        return response()->json([
            'sukses' => false,
            'isi' => 'Data dengan id : '.$id.' berhasil diupdate',
        ],404);

    }
    
}