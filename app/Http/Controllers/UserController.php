<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(){

        $posts = User::latest()->get();

        return response()->json([

            'sukses'    => true,
            'message'     => 'data daftar user berhasil ditampilkan',
            'data'      => $posts

        ]);
    }

    public function store(Request $request){
        $allRequest = $request->title;        
        $validator = Validator::make($allRequest,[
            'nama' => 'required',
            'alamat' => 'required',
            'biodata' => 'required',
            'password' => 'required',
            'post_id' => 'required',
            'comment_id' => 'required',

        ]);

        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $post = User::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'biodata' => $request->biodata,
            'password' => $request->password,
            'post_id' => $request->post_id,
            'comment_id' => $request->post_id,

        ]);

        return response()->json([
            'sukses'    => true,
            'message'     => 'data daftar user berhasil ditampilkan',
            'data'      => $post
        ],200);

        return response()->json([
            'sukses'    => false,
            'message'     => 'data daftar user berhasil ditampilkan',
        ],409);

    }

    public function show($id){

        $post = User::find($id);

        if($post){
            return response()->json([
                'sukses'    => true,
                'message'     => 'data role berhasil ditampilkan',
                'data'      => $post
            ],200);
        }

        return response()->json([
            'sukses' => false,
            'message' => 'Data dengan id : '.$id.' berhasil diupdate',
        ],404);

    }

    public function update(Request $request, $id){
        $allRequest = $request->all();

        $validator = Validator::make($allRequest,[
            'nama' => 'required',
            'alamat' => 'required',
            'biodata' => 'required',
            'password' => 'required',
            'post_id' => 'required',
            'comment_id' => 'required',

        ]);

        if ($validator->fails()){
            return response()->json($validator->errors(),400);
        }

        $post = User::find($id);

        if($post){

            $post->update([
                'nama' => 'required',
                'alamat' => 'required',
                'biodata' => 'required',
                'password' => 'required',
                'post_id' => 'required',
                'comment_id' => 'required',
    
            ]);

            return response()->json([
                'sukses' => true,
                'message' => 'Data dengan judul : '.$post->nama.' berhasil diupdate',
                'data' => $post
            ]);
        }

        return response()->json([
            'sukses' => false,
            'message' => 'Data dengan id : '.$id.' berhasil diupdate',
        ],404);
    }

    public function destroy($id){
        $post = User::find($id);

        if($post){

            $post->delete();


            return response()->json([
                'sukses'    => true,
                'message'     => 'data user berhasil didelete',
                'data'      => $post
            ],200);
        }

        return response()->json([
            'sukses' => false,
            'message' => 'Data dengan id : '.$id.' berhasil diupdate',
        ],404);

    }
    
}