<?php

namespace App\Http\Controllers\Auth;

use App\OtpCode;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\VerifiesEmails;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        
        
        $validator = Validator::make($allRequest,[
            'otp' => 'required',

        ]);

        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $otp_code = OtpCode::where('otp',$request->otp)->first();

        if (!$otp_code){
            return response()->json([
                'sukses' => false,
                'pesan' => 'OTP Code tidak ditemukan'
            ], 400);
        }

        $now = Carbon::now();
        if($now > $otp_code->valid_until){
            return response()->json([
                'sukses' => false,
                'pesan' => 'OTP Code tidak berlaku lagi'
            ],400);
        }

        $user = User::find($otp_code->user_id);
        $user->update([
            'email_verified_at' => $now
        ]);

        $otp_code->delete();

        return response()->json([
            'sukses' => true,
            'pesan' => 'User berhasil diverifikasi',
            'data' => $user
        ]);

    }

}
