<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function __invoke(Request $request)
    {
        //dd('masuk ke login');
        $allRequest = $request->all();
        
        
        $validator = Validator::make($allRequest,[
            'email' => 'required',
            'password' => 'required'

        ]);

        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'sukses' => false,
                'pesan' => 'Email atau Password tidak ditemukan',
            ], 401);
        }

        return response()->json([
            'sukses' => true,
            'message' => 'user berhasil login',
            'data' => [
                'user' => auth()->user(),
                'token' => $token
            ]
        ], 401);
    }
}