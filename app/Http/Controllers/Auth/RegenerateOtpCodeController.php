<?php

namespace App\Http\Controllers\Auth;

use App\OtpCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        
        
        $validator = Validator::make($allRequest,[
            'email' => 'required',

        ]);

        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if($user->otp_code){
            $user->otp_code()->delete();
        }

        do {
            $random = mt_rand( 100000, 999999);
            $check = OtpCode::where('otp',$random)->first();

        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinute(5),
            'user_id' => $user->id
        ]);

        return response()->json([
            'sukses' => true,
            'pesan' => 'Otp Code berhasil digenerate',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);



    }
}
