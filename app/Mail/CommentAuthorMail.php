<?php

namespace App\Mail;

use App\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentAuthorMail extends Mailable
{
    use Queueable, SerializesModels;

    public $comments;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Comment $comments)
    {
        $this->comment = $comments;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.comment.comment_author_mail')
                    ->subject('Full Stack Web Dev PKS DS');
    }
}
