<?php

namespace App;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    protected $fillable = ['name','id'];

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot(){

        parent::boot();

        //event utk menambahkan data baru
        static::creating(function($model){
            if( empty($model->id) ){
                $model->id = Str::uuid();
            }
        });
    }

}